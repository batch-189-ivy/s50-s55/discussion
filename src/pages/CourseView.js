import {useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView() {


	const { user } = useContext(UserContext);

	//Redirect a user to a different pagee after eeroll
	const navigate = useNavigate()

	//The "useParams"
	const{courseId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)


	const enroll = (courseId) => {

		fetch('http://localhost:4000/users/enroll', {
			method:"POST",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "success",
					text: "You have successfully enrolled for this course!"
				})

				navigate("/courses")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}

		})
	}



	useEffect(() => {
		console.log(courseId)

		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)


		})

	}, [courseId])


	return(
		<Container>
			<Row>
				<Col lg={{span:6, offset:3}}> 
					<Card>
		            	<Card.Body className="text-center">
			                <Card.Title>{name}</Card.Title>
			                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTONwWFn6ksUg23FZTzrroY2ws6b7kQ14_2tQ&usqp=CAU" />
			                <Card.Subtitle>Description:</Card.Subtitle>
			                <Card.Text>{description}</Card.Text>
			                <Card.Subtitle>Price:</Card.Subtitle>
			                <Card.Text>PhP {price}</Card.Text>
			                <Card.Text>Class Schedule: </Card.Text>
			                <Card.Text>8AM - 5PM</Card.Text>
			                {
			                	user.id !== null 

			                	?
			                	
			                	<Button variant="primary" onClick={() => enroll(courseId)} >Enroll</Button>

			                	:

			                	<Link className="btn btn-danger" to="/login"> Log in to Enroll </Link>
			                }
		                </Card.Body>
		              </Card>
				</Col>
			</Row>
		</Container>

		)



}
