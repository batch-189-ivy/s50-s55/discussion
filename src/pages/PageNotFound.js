import { Link } from 'react-router-dom';
import {Button} from 'react-bootstrap';

export default function PageNotFound() {
  return (
    <div>
      <h2>404 Page not found</h2>
      <h4>Go back to the <Button as={Link} to="/" variant="primary">Home!</Button></h4>
    </div>
  );
}
