import { useState, useEffect } from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


//SIMPLE SOLUTION


export default function CourseCard({courseProp}) {

	/*console.log(props)
	console.log(typeof props)
	console.log(props.courseProp.name)*/

	//console.log(courseProp.name)

	/*
	Use the state hook for this component to be able to store its state.
	States are used to keep track of information related to our individual components


	Syntax: 
		const [getter, setter] = useState(initialGetterValue)

	*/


	// const [count, setCount] = useState(0)
	// const [seats, setSeats] = useState(30)

	/*function enroll() {
		//if condition can either: count = 30 or seat = 0
		if(count === 30) {
			alert("No more seats!")
		} else {
			setCount(count + 1)
			console.log('Enrollees: ' + count + 1)

			seatCount(seat - 1)
			console.log('Seats: ' + seat - 1) 
		}

	}*/

	 // function enroll() {
  //      // if (seats > 0) {
  //           setCount(count + 1);
  //           console.log('Enrollees: ' + count);
  //           setSeats(seats - 1);
  //           console.log('Seats: ' + seats);
  //      //   } else {
  //      //      alert("No more seats available");
  //      // }; 

  //   }


    // useEffect(() => {
    // 	if (seats === 0) {
    // 		alert('No more seats available!')
    // 	}
    // }, [seats])





	//Deconstruct the course properties into their own variables
	const { name, description, price, _id} = courseProp;
	console.log(courseProp)

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    )
}



/*

Alternative:

 <Link className="btn btn-primary" to="/courseView">Details</Link>


*/





